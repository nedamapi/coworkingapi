﻿using Coworkin.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Contracts.Repositories
{
    public interface IServiceRepository : IRespository<ServicesEntity>
    {
        Task<ServicesEntity> Update(ServicesEntity entity);
    }
}
