﻿using Coworkin.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Contracts.Repositories
{
    public interface IRoomRepository : IRespository<RoomEntity>
    {
        Task<RoomEntity> Update(RoomEntity entity);
    }
}
