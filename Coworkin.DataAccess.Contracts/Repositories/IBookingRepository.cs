﻿using Coworkin.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Contracts.Repositories
{
    public interface IBookingRepository : IRespository<BookingEntity>
    {
        Task<BookingEntity> Update(BookingEntity entity);
    }
}
