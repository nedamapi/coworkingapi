﻿using Coworkin.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess.Contracts.Repositories
{
    public interface IUserRepository : IRespository<UsersEntity>
    {
    }
}
