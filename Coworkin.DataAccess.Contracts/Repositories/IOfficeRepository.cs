﻿using Coworkin.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Contracts.Repositories
{
    public interface IOfficeRepository : IRespository<OfficeEntity>
    {
        Task<OfficeEntity> Update(OfficeEntity entity);
    }
}
