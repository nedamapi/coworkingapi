﻿using Coworkin.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Contracts.Repositories
{
    public interface IAdminRepository : IRespository<AdminEntity>
    {
        Task<AdminEntity> Update(AdminEntity entity);

    }
}
