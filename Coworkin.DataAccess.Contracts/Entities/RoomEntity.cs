﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess.Contracts.Entities
{
    public class RoomEntity
    {
        public int Id { get; set; }
        public string nombre { get; set; }
        public int capacity { get; set; }

        public virtual ICollection<Offices2RoomEntity> Offices2Room { get; set; }
        public virtual ICollection<Room2ServiceEntity> Room2Services { get; set; }

    }
}
