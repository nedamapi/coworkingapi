﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Coworkin.DataAccess.Contracts.Entities
{
    public class OfficeEntity
    {
        [Key]
        public int Id { get; set; }
        public string name { get; set; }
        public string adress { get; set; }
        public string phone { get; set; }
        public string City { get; set; }
        public bool active { get; set; }
        public int IdAdmin { get; set; }
        public bool HasIndividualWorkspace { get; set; }
        public int NumberWorkspace { get; set; }
        public decimal priceWorkspaceDaily { get; set; }
        public decimal priceWorkspaceMonthly { get; set; }
        public virtual BookingEntity Booking { get; set; }
        public virtual AdminEntity Admin { get; set; }
        public virtual ICollection<Offices2RoomEntity> Offices2Room { get; set; }

    }
}
