﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess.Contracts.Entities
{
    public class Room2ServiceEntity
    {
        public int roomId{ get; set; }
        public int serviceId { get; set; }

        public virtual ServicesEntity Services { get; set; }
        public virtual RoomEntity Room { get; set; }
    }
}
