﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Coworkin.DataAccess.Contracts.Entities
{
    public class AdminEntity
    {
        [Key]
        public int Id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
}
