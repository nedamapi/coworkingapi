﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess.Contracts.Entities
{
    public class BookingEntity
    {
        public int Id { get; set; }
        public int userId { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime createDate { get; set; }
        public int OfficeId { get; set; }
        public bool RentWorkspace { get; set; }
        public int? RoomId { get; set; }

        public virtual UsersEntity Users { get; set; }
        public virtual OfficeEntity Office { get; set; }
    }
}
