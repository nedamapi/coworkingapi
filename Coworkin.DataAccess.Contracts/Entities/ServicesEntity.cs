﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Coworkin.DataAccess.Contracts.Entities
{
    public class ServicesEntity
    {
        [Key]
        public int Id { get; set; }
        public string nombre { get; set; }
        public bool active { get; set; }
        public decimal price { get; set; }
        public virtual ICollection<Room2ServiceEntity> Room2Services { get; set; }
    }
}
