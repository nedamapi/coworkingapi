﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Coworkin.DataAccess.Contracts.Entities
{
    public class UsersEntity
    {
        [Key]
        public int Id { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string email{ get; set; }
        public bool active { get; set; }
        public DateTime createDate { get; set; }

        public virtual BookingEntity Booking { get; set; }
    }
}
