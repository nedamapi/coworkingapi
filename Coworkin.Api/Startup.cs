﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coworkin.Api.Config;
using Coworkin.CrossCutting.Register;
using Coworkin.DataAccess;
using Coworkin.DataAccess.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Coworkin.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CoworkingContext>
                (options => options.UseSqlServer
                    (
                        Configuration.GetConnectionString("DatabaseConnection")
                    )
                );

            services.AddScoped<ICoworkingContext, CoworkingContext>();

            IoCRegister.AddRegistration(services);
            SwaggerConfig.AddRegistration(services);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            SwaggerConfig.AddRegistration(app);

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
