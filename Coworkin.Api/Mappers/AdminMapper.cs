﻿using Coworkin.Api.ViewModels;
using Coworkin.Bussiness.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coworkin.Api.Mappers
{
    public static class AdminMapper
    {
        public static Admin Map(AdminViewModel model)
        {
            return new Admin()
            {
                email = model.email,
                HireDate = DateTime.Now.AddDays(5),
                name = model.name,
                phone = model.phone
            };
        }
    }
}
