﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coworkin.Api.Mappers;
using Coworkin.Api.ViewModels;
using Coworkin.Application.Contracts.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Coworkin.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [HttpGet("[Action]/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var name = await _adminService.GetAdminName(id);

            return Ok(name);
        }

        [HttpPost("[Action]")]
        public async Task<IActionResult> AddAdmin([FromBody] AdminViewModel model)
        {
            var admin = await _adminService.AddAdmin(AdminMapper.Map(model));

            return Ok(admin);
        }

    }
}