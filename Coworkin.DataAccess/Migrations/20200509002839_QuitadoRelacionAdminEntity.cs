﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Coworkin.DataAccess.Migrations
{
    public partial class QuitadoRelacionAdminEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offices_Admins_IdAdmin",
                table: "Offices");

            migrationBuilder.DropIndex(
                name: "IX_Offices_IdAdmin",
                table: "Offices");

            migrationBuilder.AddColumn<int>(
                name: "AdminId",
                table: "Offices",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Offices_AdminId",
                table: "Offices",
                column: "AdminId");

            migrationBuilder.AddForeignKey(
                name: "FK_Offices_Admins_AdminId",
                table: "Offices",
                column: "AdminId",
                principalTable: "Admins",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Offices_Admins_AdminId",
                table: "Offices");

            migrationBuilder.DropIndex(
                name: "IX_Offices_AdminId",
                table: "Offices");

            migrationBuilder.DropColumn(
                name: "AdminId",
                table: "Offices");

            migrationBuilder.CreateIndex(
                name: "IX_Offices_IdAdmin",
                table: "Offices",
                column: "IdAdmin",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Offices_Admins_IdAdmin",
                table: "Offices",
                column: "IdAdmin",
                principalTable: "Admins",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
