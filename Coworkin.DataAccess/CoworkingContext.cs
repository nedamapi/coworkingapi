﻿using Coworkin.DataAccess.Contracts;
using Coworkin.DataAccess.Contracts.Entities;
using Coworkin.DataAccess.EntityConfig;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess
{
    public class CoworkingContext : DbContext, ICoworkingContext
    {
        public DbSet<UsersEntity> Users { get; set; }
        public DbSet<AdminEntity> Admins {get; set;}
        public DbSet<BookingEntity> Bookings { get; set; }
        public DbSet<OfficeEntity> Offices { get; set; }
        public DbSet<Offices2RoomEntity> offices2Rooms { get; set; }
        public DbSet<RoomEntity> Rooms { get; set; }
        public DbSet<Room2ServiceEntity> room2Services { get; set; }
        public DbSet<ServicesEntity> Services { get; set; }

        public CoworkingContext(DbContextOptions options) : base(options) { }

        public CoworkingContext()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            AdminEntityConfig.SetEntityBuilder(modelBuilder.Entity<AdminEntity>());
            UserEntityConfig.SetEntityBuilder(modelBuilder.Entity<UsersEntity>());
            BookingEntityConfig.SetEntityBuilder(modelBuilder.Entity<BookingEntity>());
            OfficeEntityConfig.SetEntityBuilder(modelBuilder.Entity<OfficeEntity>());
            Office2RoomEntityConfig.SetEntityBuilder(modelBuilder.Entity<Offices2RoomEntity>());
            RoomEntityConfig.SetEntityBuilder(modelBuilder.Entity<RoomEntity>());
            Room2ServicesEntityConfig.SetEntityConfig(modelBuilder.Entity<Room2ServiceEntity>());
            ServicesEntityConfig.SetEntityBuilder(modelBuilder.Entity<ServicesEntity>());

            base.OnModelCreating(modelBuilder);
        }
    }
}
