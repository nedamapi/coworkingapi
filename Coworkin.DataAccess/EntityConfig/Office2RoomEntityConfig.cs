﻿using Coworkin.DataAccess.Contracts.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess.EntityConfig
{
    public class Office2RoomEntityConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<Offices2RoomEntity> entityBuilder)
        {
            entityBuilder.ToTable("Office2Rooms");

            entityBuilder.HasOne(x => x.Office).WithMany(x => x.Offices2Room).HasForeignKey(x => x.OfficeId);
            entityBuilder.HasOne(x => x.Room).WithMany(x => x.Offices2Room).HasForeignKey(x => x.RoomId);

            entityBuilder.HasKey(x => new { x.RoomId, x.OfficeId });
            entityBuilder.Property(x => x.OfficeId).IsRequired();
            entityBuilder.Property(x => x.RoomId).IsRequired();
        }
    }
}
