﻿using Coworkin.DataAccess.Contracts.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess.EntityConfig
{
    public class Room2ServicesEntityConfig
    {
        public static void SetEntityConfig(EntityTypeBuilder<Room2ServiceEntity> entityBuilder)
        {
            entityBuilder.ToTable("Room2Services");

            entityBuilder.HasOne(x => x.Room).WithMany(x => x.Room2Services).HasForeignKey(x => x.roomId);
            entityBuilder.HasOne(x => x.Services).WithMany(x => x.Room2Services).HasForeignKey(x => x.serviceId);

            entityBuilder.HasKey(x => new { x.roomId, x.serviceId });

            entityBuilder.Property(x => x.serviceId).IsRequired();
            entityBuilder.Property(x => x.roomId).IsRequired();
        }
    }
}
