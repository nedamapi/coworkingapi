﻿using Coworkin.DataAccess.Contracts.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess.EntityConfig
{
    public class ServicesEntityConfig
    {
        public static void SetEntityBuilder(EntityTypeBuilder<ServicesEntity> entityBuilder)
        {
            entityBuilder.ToTable("Services");
            entityBuilder.HasKey(x => x.Id);
            entityBuilder.Property(x => x.Id).IsRequired();
        }
    }
}
