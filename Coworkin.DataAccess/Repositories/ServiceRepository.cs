﻿using Coworkin.DataAccess.Contracts;
using Coworkin.DataAccess.Contracts.Entities;
using Coworkin.DataAccess.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Repositories
{
    public class ServiceRepository : IServiceRepository
    {
        public readonly ICoworkingContext _coworkingContext;

        public ServiceRepository(ICoworkingContext coworkingContext)
        {
            _coworkingContext = coworkingContext;
        }

        public async Task<ServicesEntity> Add(ServicesEntity entity)
        {

            await _coworkingContext.Services.AddAsync(entity);

            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task<ServicesEntity> Update(ServicesEntity entity)
        {
            var updateEntity = _coworkingContext.Services.Update(entity);

            await _coworkingContext.SaveChangesAsync();

            return updateEntity.Entity;
        }

        public async Task<ServicesEntity> Get(int idEntity)
        {
            var result = await _coworkingContext.Services
                .FirstOrDefaultAsync(x => x.Id == idEntity);

            return result;
        }

        public async Task<ServicesEntity> Update(int idEntity, ServicesEntity updateEnt)
        {
            var entity = await Get(idEntity);
            entity.nombre = updateEnt.nombre;

            _coworkingContext.Services.Update(entity);
            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(int idEntity)
        {
            var entity = await _coworkingContext.Services.SingleAsync(x => x.Id == idEntity);
            _coworkingContext.Services.Remove(entity);

            await _coworkingContext.SaveChangesAsync();

        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ServicesEntity>> GetAll()
        {
            throw new NotImplementedException();
        }


    }
}
