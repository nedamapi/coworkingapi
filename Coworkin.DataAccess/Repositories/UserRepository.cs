﻿using Coworkin.DataAccess.Contracts;
using Coworkin.DataAccess.Contracts.Entities;
using Coworkin.DataAccess.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Repositories
{
    public class UserRepository : IUserRepository
    {
        public readonly ICoworkingContext _coworkingContext;

        public UserRepository(ICoworkingContext coworkingContext)
        {
            _coworkingContext = coworkingContext;
        }

        public async Task<UsersEntity> Add(UsersEntity entity)
        {

            await _coworkingContext.Users.AddAsync(entity);

            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task<UsersEntity> Update(UsersEntity entity)
        {
            var updateEntity = _coworkingContext.Users.Update(entity);

            await _coworkingContext.SaveChangesAsync();

            return updateEntity.Entity;
        }

        public async Task<UsersEntity> Get(int idEntity)
        {
            var result = await _coworkingContext.Users
                .FirstOrDefaultAsync(x => x.Id == idEntity);

            return result;
        }

        public async Task<UsersEntity> Update(int idEntity, UsersEntity updateEnt)
        {
            var entity = await Get(idEntity);
            entity.name = updateEnt.name;

            _coworkingContext.Users.Update(entity);
            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(int idEntity)
        {
            var entity = await _coworkingContext.Users.SingleAsync(x => x.Id == idEntity);
            _coworkingContext.Users.Remove(entity);

            await _coworkingContext.SaveChangesAsync();

        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<UsersEntity>> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
