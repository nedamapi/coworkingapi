﻿using Coworkin.DataAccess.Contracts;
using Coworkin.DataAccess.Contracts.Entities;
using Coworkin.DataAccess.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        //CRUD

        private readonly ICoworkingContext _coworkingContext;

        public AdminRepository(ICoworkingContext coworkingContext)
        {
            _coworkingContext = coworkingContext;
        }

        public async Task<AdminEntity> Add(AdminEntity entity)
        {

            await _coworkingContext.Admins.AddAsync(entity);

            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task<AdminEntity> Update(AdminEntity entity)
        {
            var updateEntity = _coworkingContext.Admins.Update(entity);

            await _coworkingContext.SaveChangesAsync();

            return updateEntity.Entity;
        }

        public async Task<AdminEntity> Get(int idEntity)
        {
            var result = await _coworkingContext.Admins
                .FirstOrDefaultAsync(x => x.Id == idEntity);

            return result;
        }

        public async Task<AdminEntity> Update(int idEntity, AdminEntity updateEnt)
        {
            var entity = await Get(idEntity);
            entity.name = updateEnt.name;

            _coworkingContext.Admins.Update(entity);
            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync (int idEntity)
        {
            var entity = await _coworkingContext.Admins.SingleAsync(x => x.Id == idEntity);
             _coworkingContext.Admins.Remove(entity);

            await _coworkingContext.SaveChangesAsync();

        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AdminEntity>> GetAll()
        {
            return _coworkingContext.Admins.Select(x => x);
        }

    }
}
