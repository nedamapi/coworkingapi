﻿using Coworkin.DataAccess.Contracts;
using Coworkin.DataAccess.Contracts.Entities;
using Coworkin.DataAccess.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Repositories
{
    public class OfficeRepository : IOfficeRepository
    {
        private readonly ICoworkingContext _coworkingContext;

        public OfficeRepository (ICoworkingContext coworkingContext)
        {
            _coworkingContext = coworkingContext;
        }

        public async Task<OfficeEntity> Add(OfficeEntity entity)
        {
            await _coworkingContext.Offices.AddAsync(entity);

            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task<OfficeEntity>Update (OfficeEntity entity)
        {
            var updatedValue = _coworkingContext.Offices.Update(entity);

            await _coworkingContext.SaveChangesAsync();

            return updatedValue.Entity;
        }

        public async Task<OfficeEntity> Update (int id, OfficeEntity entity)
        {
            var updatedValue = await Get(id);

            entity.adress = updatedValue.adress;

            _coworkingContext.Update(entity);

            return entity;
        }

        public async Task<OfficeEntity> Get(int idEntity)
        {
            var result = await _coworkingContext.Offices.FirstOrDefaultAsync(x => x.Id == idEntity);
            return result;
        }

        public async Task DeleteAsync(int idEntity)
        {
            var deleted = await _coworkingContext.Offices.SingleAsync(x => x.Id == idEntity);

             _coworkingContext.Offices.Remove(deleted);

            await _coworkingContext.SaveChangesAsync();

        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<OfficeEntity>> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
