﻿using Coworkin.DataAccess.Contracts;
using Coworkin.DataAccess.Contracts.Entities;
using Coworkin.DataAccess.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Repositories
{
    public class BookingRepository : IBookingRepository
    {
        public readonly ICoworkingContext _coworkingContext;

        public BookingRepository(ICoworkingContext coworkingContext)
        {
            _coworkingContext = coworkingContext;
        }

        public async Task<BookingEntity> Add(BookingEntity entity)
        {

            await _coworkingContext.Bookings.AddAsync(entity);

            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task<BookingEntity> Update(BookingEntity entity)
        {
            var updateEntity = _coworkingContext.Bookings.Update(entity);

            await _coworkingContext.SaveChangesAsync();

            return updateEntity.Entity;
        }

        public async Task<BookingEntity> Get(int idEntity)
        {
            var result = await _coworkingContext.Bookings
                .Include(x => x.Office)
                .FirstOrDefaultAsync(x => x.Id == idEntity);

            return result;
        }

        public async Task<BookingEntity> Update(int idEntity, BookingEntity updateEnt)
        {
            var entity = await Get(idEntity);
            entity.RentWorkspace = updateEnt.RentWorkspace;

            _coworkingContext.Bookings.Update(entity);
            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(int idEntity)
        {
            var entity = await _coworkingContext.Bookings.SingleAsync(x => x.Id == idEntity);
            _coworkingContext.Bookings.Remove(entity);

            await _coworkingContext.SaveChangesAsync();

        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<BookingEntity>> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
