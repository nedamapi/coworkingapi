﻿using Coworkin.DataAccess.Contracts;
using Coworkin.DataAccess.Contracts.Entities;
using Coworkin.DataAccess.Contracts.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.DataAccess.Repositories
{
    public class RoomRepository : IRoomRepository
    {
        private readonly ICoworkingContext _coworkingContext;

        public RoomRepository(ICoworkingContext coworkingContext)
        {
            _coworkingContext = coworkingContext;
        }

        public async Task<RoomEntity> Add(RoomEntity entity)
        {
            await _coworkingContext.Rooms.AddAsync(entity);

            await _coworkingContext.SaveChangesAsync();

            return entity;
        }

        public async Task<RoomEntity> Update(RoomEntity entity)
        {
            var updatedValue = _coworkingContext.Rooms.Update(entity);

            await _coworkingContext.SaveChangesAsync();

            return updatedValue.Entity;
        }

        public async Task<RoomEntity> Update(int id, RoomEntity entity)
        {
            var updatedValue = await Get(id);

            entity.nombre = updatedValue.nombre;

            _coworkingContext.Update(entity);

            return entity;
        }

        public async Task<RoomEntity> Get(int idEntity)
        {
            var result = await _coworkingContext.Rooms.FirstOrDefaultAsync(x => x.Id == idEntity);
            return result;
        }

        public async Task DeleteAsync(int idEntity)
        {
            var deleted = await _coworkingContext.Rooms.SingleAsync(x => x.Id == idEntity);

            _coworkingContext.Rooms.Remove(deleted);

            await _coworkingContext.SaveChangesAsync();

        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<RoomEntity>> GetAll()
        {
            throw new NotImplementedException();
        }

    }
}
