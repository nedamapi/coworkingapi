﻿using Coworkin.Bussiness.Models;
using Coworkin.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.DataAccess.Mappers
{
    public static class AdminMapper
    {
        public static AdminEntity Map(Admin dto)
        {
            return new AdminEntity()
            {
                email = dto.email,
                Id = dto.Id,
                name = dto.name,
                phone = dto.phone
            };
        }

        public static Admin Map(AdminEntity dto)
        {
            return new Admin()
            {
                email = dto.email,
                Id = dto.Id,
                name = dto.name,
                phone = dto.phone
            };
        }
    }
}
