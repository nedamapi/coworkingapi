﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coworkin.Bussiness.Models
{
    public class Admin
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }

        public DateTime HireDate { get; set; }
    }
}
