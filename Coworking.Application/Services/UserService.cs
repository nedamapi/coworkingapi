﻿using Coworkin.Application.Contracts.Services;
using Coworkin.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IAdminService _adminService;
        private readonly IUserRepository _userRepository;

        public UserService(IAdminService adminService, IUserRepository userRepository)
        {
            _adminService = adminService;
            _userRepository = userRepository;
        }

        public async Task GetUserName(int idUser, int idAdmin)
        {
            var user = await _userRepository.Get(idUser);

            var adminName = await _adminService.GetAdminName(idAdmin);
        }
    }
}
