﻿using Coworkin.Application.Contracts.Services;
using Coworkin.Bussiness.Models;
using Coworkin.DataAccess.Contracts.Repositories;
using Coworkin.DataAccess.Mappers;
using Remotion.Linq.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Coworkin.Application.Services
{
    public class AdminService : IAdminService
    {
        private readonly IAdminRepository _adminRepository;

        public AdminService(IAdminRepository adminRepository)
        {
            _adminRepository = adminRepository;
        }

        public async Task<string> GetAdminName(int id)
        {
            var entidad = await _adminRepository.Get(id);

            return entidad.name;
        }

        public async Task<Admin> AddAdmin(Admin admin)
        {
            var addedEntity = await _adminRepository.Add(AdminMapper.Map(admin));

            return AdminMapper.Map(addedEntity);
        }


    }
}
